
\subsection{Limits and Continuity}
\begin{definition}
  Let $(x_n)_{n\in \N}$ be a sequence of real numbers. The sequence is said to 
  \emph{converge} to a real number $x\in \R$ if for every $\varepsilon >0$ 
  there exists an $N\in \N$ such that 
  \[
     | x_n - x | < \varepsilon \: \text{for all natural } n \geq N \ . 
  \]
  One then calls $(x_n)_{n\in \N}$ a \emph{convergent} sequence and 
  $x$ its \emph{limit}. 
\end{definition}

\begin{proposition}
  The limit of a convergent sequence is uniquely determined. 
\end{proposition}

\begin{definition}
  The function $f: \R \rightarrow \R$  has the
  \emph{limit} $b$ at the point $a \in \R$, written 
  \[
    \lim_{x \rightarrow a} f(x) = b ,
  \]
  if for every $\varepsilon >0$  there is a $\delta >0$ such that 
  \[
      | f (x) - b | < \varepsilon \text{ for all } x \neq a
      \text{ with } | x - a | < \delta \ .
  \] 
\end{definition}
\begin{remark}
  Intuitively,  $\lim_{x \rightarrow a} f(x) = b$ means that 
  $f(x)$ is as close to $b$ as we wish whenever the distance 
  of the point $x$ to $a$ is sufficiently small.  
\end{remark}
\begin{definition}
  A function $f: D \rightarrow \R$ defined on a  subset $D \subset \R$ 
  is called \emph{continuous} at the point $a \in \R$, if 
  \[
    \lim_{x \rightarrow a} f(x = f(a) .
  \]
  \noindent
  The function $f : D \rightarrow \R$ 
  is said to be \emph{continuous on} $D$ or just \emph{continuous}
  if it is continuous  at every point $a \in D$.
\end{definition}

\subsection{Differentiability}
\begin{definition}
  A function $f:I \rightarrow \R$, $x \mapsto f(x)$ defined on an open interval
  $I\subset \R$  is called 
  \emph{differentiable in} 
  $a \in I$ if the limit
\begin{equation*}
  \begin{split}
   & f'(x) := \lim_{h\rightarrow 0} \frac{ f (a+h) - f(a)}{h} \\
  \end{split}
\end{equation*}
exists. One then calls  $f' (x)$ the \emph{derivative} of $f$ at $a$. 
The derivative of a  function $f$ differentiable at $a$ is sometimes also
denoted by $Df (a)$ or $\frac{d f}{d x} (a)$. 
If $f$  is differentiable in every  point of its domain $I$, then one says that $f$ is \emph{differentiable}.
\end{definition} 

\begin{proposition}
  Let $f,g:I \to \R$ be two functions defined on the open interval 
  $I \subset \R$. Ass ume that both $f,g$ are differentiable in the point 
  $a\in I$. Then the following holds true:
  \begin{letterlist}
  \item The sum $f+g :I \to \R$, $x \mapsto f(x)+g(x)$
        is differentiable in $a$ with derivate  given by 
        $(f+g)'(a)=f'(a)+g'(a)$. 
  \item For every $c\in \R$  the scalar multiple 
        $cf :I \to \R$, $x \mapsto c \, f(x)$
        is differentiable in $a$ with derivate  given by 
        $(cf)'(a)=c\, f'(a)$. 
  \item \textup{(}Product rule\textup{)}
        The product $f \cdot g :I \to \R$, $x \mapsto f(x)\cdot g(x)$
        is differentiable in $a$ with derivative  given by 
        $(f\cdot g)'(a)=f'(a) \cdot g(a) + f(a) \cdot g'(a)$.
  \end{letterlist}
\end{proposition}

\begin{proposition}
  Let $f:I \to \R$ and $g:J \to \R $ be two functions defined on open intervals 
  $I,J \subset \R$. Assume that $f(I) \subset J$.            
  If $f$ is differentiable in some point $a \in I$ and $g$ is differentiable 
  in the pont $f(a)$, then the composition 
  $g\circ f : I \to \R$, $x \mapsto g(f(x))$ is differentiable in $a$ with
  derivative 
  \[
    (g\circ f)'(a) = g'(f(a)) \cdot f'(a) \ . 
  \]
\end{proposition}

\begin{examples}
The following is a list of differentiable functions and their derivatives.
\begin{letterlist}
\item
  Every polynomial function 
  \[ 
    p:\R\to\R, \quad x \mapsto p(x)= \sum_{k=0}^n a_k x^k \ ,       
  \]
  where $a_0,\ldots, a_n \in \R$ are its coefficients, 
  is differentiable and has derivative 
  \[
     p' : \R \to \R, \quad x \mapsto \sum_{k=1}^n k \, a_k x^{k-1} \ .
  \]
  In particular the monomials $q_n: \R \to \R$, $x \mapsto x^n$ with $n\in \N$  
  are differentiable with derivatives given by 
  $q_n': \R \to\R$, $x \mapsto n \, x^{n-1}$. 
\item 
  The trigonometric functions $\sin$, $\cos$, $\tan$, $\cot$ are all 
  differentiable on their natural domains. The derivatives are given 
  by
  \begin{equation*}
  \begin{split}
    \sin'(x) & = \cos x  \hspace{8mm}  \text{for } x \in \R,\\
    \cos'(x) & = -\sin x    \hspace{4.7mm} \text{for } x \in \R,\\
    \tan'(x) & = \frac{1}{\cos^2 x}   \hspace{5.5mm} \text{for } x \in 
    \R \setminus \{ (2k+1)\frac{\pi}{2} \mid k \in \N \}, \\
    \cot'(x) & = \frac{-1}{\sin^2 x}   \hspace{6mm} \text{for } x \in 
    \R \setminus \{ k \pi \mid k \in \N \}.
  \end{split}
  \end{equation*}
\end{letterlist}
\end{examples}

\subsection{Basic curve analysis}

\begin{definition} (Symmetries of a function)
  A function $f:\R \to \R$, $x \mapsto y =f(x)$ is called \emph{even} if its 
  graph is \emph{symmetric to the} $y$-axis meaning that 
  $f(-x)=f(x)$ for all $x\in \R$. 
  The function $f$ is called  \emph{odd} if its graph is 
  \emph{symmetric to the origin} that is if $f(-x)=-f(x)$ for all $x\in \R$.
\end{definition}

\begin{definition}
   A function $f:D \rightarrow \R$ defined on a subset $D \subset \R$
   is called \emph{strictly monotone increasing}, if $f(x') < f(x)$ 
   whenever $x' < x $ for two points $x,x'\in D$.
   If instead $f(x) < f(x')$ for all points  $x,x'\in D$ with  $x' < x $,
   then $f$ is called \emph{strictly monotone decreasing}.
\end{definition}

\begin{proposition}
   Let $f:I \to \R$ be a differentiable function defined on the 
   open interval $I$. 
   \begin{letterlist}
   \item   
      If $f'(x) >0$ for all $x \in I$, then 
      $f$ is strictly monotone increasing, if $f'(x) <0$ for all $x\in I$, 
      then $f$ is strictly monotone decreasing. 
   \item
      If for some $x\in I$ the derivative of $f$ in $x$ vansihes that is if 
      $f'(x) =0$, then the grap of $f$ has in $x$ a horizontal tangent.  
   \end{letterlist}
  
\end{proposition}

\begin{definition}
   Let $f:I \rightarrow \R$, $x\mapsto f(x)$ be a function defined
   on an open interval $I\subset \R$. 
   A point $a\in I$ is called a \emph{relative maximum} 
   (respectively a \emph{relative minimum}) of $f$, if
   $f(x)\leq f(a)$ (respectively $f(x)\geq f(a)$) for all $x$ 
   in an $\varepsilon$-neighborhood $U_\varepsilon (a) \subset I $ of $a$.

   The point $a\in \R^n$ is called a \emph{global maximum} (resp.~ \emph{global minimum}) of $f$
   over the region $R\subset \R^n$ , if $f(x)\leq f(a)$ (resp.~$f(x)\geq f(a)$) for all $x\in R$.
\end{definition}

\begin{theorem}
  Assume that $f:\R^2 \rightarrow \R$, $(x,y)\mapsto f(x,y)$ is a twice continuously partially 
  differentiable function.
  Suppose that $(a,b)$ is a point where $\grad f(a,b)=0$. Let 
  \[
        D = \dfrac{\partial^2f}{\partial x^2}(a,b)\cdot \dfrac{\partial^2f}{\partial y^2}(a,b) -
            \left( \dfrac{\partial^2f}{\partial x \partial y} (a,b) \right)^2 .
  \]
  \begin{itemize}
  \item If $D>0$ and $\dfrac{\partial^2f}{\partial x^2}(a,b)>0$, then $f$ has a local minimum in $a$.
  \item If $D>0$ and $\dfrac{\partial^2f}{\partial x^2}(a,b)<0$, then $f$ has a local maximum in $a$.
  \item If $D<0$, then $f$ has a saddle point in $a$.
  \item If $D=0$, no conclusion can be made: $f$ can have a local maximum, a local minimum, a saddle
  point, or none of these in the point $(a,b)$.  
  \end{itemize}
\end{theorem}
  
\begin{definition}
   If $f:\R^2 \rightarrow \R$, $(x,y)\mapsto f(x,y)$ and $g :\R^2 \rightarrow \R$, $(x,y)\mapsto g(x,y)$ 
   are functions, and $c\in \R$ a number, a point $(a,b) \in \R^2$ is called
   a \emph{local maximum} (resp.~ \emph{local minimum}) of $f$ \emph{under the constraint} $g(x,y)=c$, if
   $f(x,y)\leq f(a,b)$ (resp.~$f(x,y)\geq f(a,b)$) for all $(x,y)\in \R^2$ near $(a,b)$ 
   which satisfy $g(x,y)=c$.

   The point $(a,b)\in \R^2$ is called a \emph{global maximum} (resp.~ \emph{global minimum}) of $f$
   \emph{under the constraint} $g(x,y)=c$, if $f(x,y)\leq f(a,b)$ (resp.~$f(x,y)\geq f(a,b)$) for all 
   $(x,y)\in \R^2$  which satisfy $g(x,y)=c$.
\end{definition}

\begin{theorem}
  Assume that $f:\R^2 \rightarrow \R$, $x\mapsto f(x)$ is a smooth function, and 
  $g:\R^2 \rightarrow \R$, $x\mapsto g(x)$ a smooth constraint. 
  If $f$ has a maximum or minimum at the point $(a,b)$ under the constraint $g(x,y)=c$, 
  then $(a,b)$ either satisfies the equations
  \[
     \grad f (a,b) = \lambda \grad g (a,b) \text{ and } g(a,b)=c \text{ for some $\lambda \in \R$},
  \]
  or $\grad g (a,b)=0$.  The number $\lambda$ is called the \emph{Lagrange multiplier}.
\end{theorem}
